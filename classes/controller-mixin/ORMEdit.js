//prepare instance for edit view

const {K8, ControllerMixin} = require('@komino/k8');
const HelperForm = K8.require('helper/Form');

class ControllerMixinORMEdit extends ControllerMixin{

  async before(){
    this.clientDB = this.client.mixin.get('db');
  }

  action_edit(){
    const instance = this.client.mixin.get('instance');
    this.applyQueryValues(instance);

    this.addBehaviour('readData', Object.assign(
      this.getFieldData(instance),
      this.getBelongsTo(instance),
      this.getBelongsToMany(instance),
      this.getHasMany(instance),
      this.getFormDestination(),
      this.getDomain()
    ));
  }

  applyQueryValues(instance){
    const $_GET = this.client.request.query || {};

    if($_GET['values']){
      const values = JSON.parse($_GET['values']);
      this.client.model.fields.forEach((v,k) =>{
        if(!values[k])return;
        if(instance[k] === null){
          instance[k] = values[k];
        }
      });
    }
  }

  action_read(){
    this.action_edit()
  }

  action_create(){
    this.client.mixin.set('instance', new this.client.model(null, {database : this.clientDB}));
    this.action_edit();
  }

  getFormDestination(){
    const $_GET = this.client.request.query || {};
    if($_GET['cp']){
      return {
        destination : $_GET['cp']
      }
    }
    return {};
  }

  getFieldData(instance){
    const m = instance.constructor;

    return {
      title     : `${(instance.id) ? 'Edit': 'Create'} ${m.name}`,
      model     : m,
      item      : instance,
      fields    : [...m.fields].map(x => HelperForm.getFieldValue('', x[0], x[1], instance[x[0]])),
    };
  }

  getBelongsTo(instance) {
    const m = instance.constructor;
    if (!m.belongsTo || m.belongsTo.length <= 0) return;

    return {
      belongsTo: [...m.belongsTo].map(x => {
        const fk = x[0];
        const model = K8.require(`models/${x[1]}`);
        const mm = new model(null, {database : this.clientDB});
        return {
          instance: instance,
          model: model,
          foreign_key: fk,
          items : mm.all()
        }
      })
    }
  }

  getBelongsToMany(instance){
    const m = instance.constructor;
    if(!m.belongsToMany || m.belongsToMany.length <= 0)return;

    return {
      belongsToMany: m.belongsToMany.map( x => {
            const model = K8.require(`models/${x}`);
            const lk = m.jointTablePrefix + '_id';
            const fk = model.jointTablePrefix + '_id';
            const table = `${m.jointTablePrefix}_${model.tableName}`;
            const mm = new model(null, {database : this.clientDB});

            const items = mm.all();
            const itemsById = {};
            items.forEach(x => itemsById[x.id] = x);

            const values = mm
                .prepare(`SELECT * from ${table} WHERE ${lk} = ?`)
                .all(instance.id);

            values.forEach(x => {
              itemsById[x[fk]].linked = true;
              itemsById[x[fk]].weight = x.weight;
            }) ;

            return{
              model : model,
              values : values,
              items: items
            }
          }
      )}
  }

  getHasMany(instance){
    const m = instance.constructor;
    if(!m.hasMany || m.hasMany.length <=0 )return;

    //TODO, collapse different hasMany fk with same model
    //eg. shipping address and billing address with same address id.
    return {
      hasMany : m.hasMany.map( x => {
        const model = K8.require(`models/${x[1]}`);
        const mm = new model(null, {database : this.clientDB});

        const fk = x[0];
        const table = `${model.tableName}`;

        const items = mm.prepare(`SELECT * from ${table} WHERE ${fk} = ?`)
            .all(this.client.id)
            .map(item => Object.assign(item, {
              fields: [...model.fields]
                  .map(n => HelperForm.getFieldValue(`${model.name}(${item.id})`, n[0], n[1], item[n[0]] ||''))
            }));

        const fields = [...model.fields].map(x => ({name:x[0], type: x[1].replace(/!$/, ''), required: /!$/.test(x[1])}));

        return {
          fk: fk,
          model : model,
          fields: fields,
          items : items,
          defaultValues: encodeURIComponent(`{"${fk}":${this.client.id}}`),
          checkpoint: encodeURIComponent(this.client.request.raw.url)
        }
      })
    }
  }

  getDomain(){
    return {
      domain : this.client.request.hostname
    }
  }
}

module.exports = ControllerMixinORMEdit;