const {K8, ControllerMixin} = require('@komino/k8');
class ControllerMixinProxyTable extends ControllerMixin{
  constructor(client, databases=[]) {
    super(client);
    this.databases = databases;
  }

  async action_update(){
    const Proxy = K8.require(`models/Proxy${this.client.model.name}`)
    const id = this.client.mixin.get('instance').id;

    this.databases.forEach(database =>{
      new Proxy(null, {database: database, createWithId: id}).save();
    });
  }

  async action_delete(){
    if(!this.client.request.query['confirm'])return;

    const Proxy = K8.require(`models/Proxy${this.client.model.name}`)
    const id = this.client.id;

    this.databases.forEach(database =>{
      new Proxy(id, {database: database, lazyload: true}).delete();
    });
  }
}

module.exports = ControllerMixinProxyTable;