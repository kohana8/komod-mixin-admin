//controller to receive data

const {K8, ControllerMixin} = require('@komino/k8');
const equal = require('fast-deep-equal');
const pluralize = require('pluralize');

const readOrCreateMap = (map, key, defaultValue) =>{
  const result = map.get(key);

  if(!result){
    map.set(key, defaultValue);
    return defaultValue;
  }

  return result;
}

class ControllerMixinORMWrite extends ControllerMixin{
  async before(){
    this.clientDB = this.client.mixin.get('db');
    this.model = this.client.model;
  }

  async action_multiple_update(){
    const postData = this.client.mixin.get('$_POST');
    const instances = this.collectPostDataInstances(postData);
    //validate instances are client.model
    this.validateModel(instances);
    this.validateNoEmptyId(instances);
    //validate all instances have id

    this.multipleUpdate(instances);
  }

  createHasManyLoop(deepChildren, instance, key=''){
    if(!deepChildren.has(key))return;

    const children = deepChildren.get(key);

    this.validateChildren(instance.constructor, children);
    deepChildren.delete(key);

    children.forEach((values, type) => {
      const ins = this.createHasMany(type, values, instance);
      if(!ins){
        return;
      }
      this.createHasManyLoop(deepChildren, ins, type);
    })


  }

  async action_update(){
    const postData = this.client.mixin.get('$_POST');
    const children = this.collectPostDataInstances(postData);
    const deepChildren = this.collectDeepChildren(postData);

    this.validateChildren(this.client.model, children);

    /**
     *
     * @type {Map<string, *>}
     */
    const fieldsToUpdate = this.mappingFields();
    this.belongsFields(fieldsToUpdate);//append fk to fieldsToUpdate

    let instance;

    if(this.client.id){
      instance = this.update(fieldsToUpdate);
      this.multipleUpdate(children);
      this.createHasManyLoop(deepChildren, instance)
    }else{
      instance = this.create(fieldsToUpdate);
      this.client.id = instance.id;
      this.createHasManyLoop(deepChildren, instance)
    }

    this.add(this.client.model, this.client.id, postData);

    /*convert postData["*ModelA"] => [["ModelA", {ids: [id], postData: {":Model":[]}} ]]*/
    this.collectManyToManyForeign(postData, this.client.model, this.client.id).forEach((v, k)=>{
      const m = K8.require(`models/${k}`);
      v.ids.forEach(id => {
        //should not delete many to many from Foreign
        this.add(m, id, v.postData, true);
      })
    })

    this.addBehaviour('fieldsToUpdate', fieldsToUpdate);
    this.addBehaviour('instance', instance);
  }

  /**
   *
   * @param {Map} fieldsToUpdate
   */
  belongsFields(fieldsToUpdate){
    const $_POST  = this.client.mixin.get('$_POST');
    const belongs = this.client.model.belongsTo;
    if(!belongs || belongs.length <= 0)return;

    belongs.forEach((v , k) => {
      const value = $_POST[`:${k}`];
      if(value === undefined)return;
      if(value === '')return;
      if(Array.isArray(value))return;

      fieldsToUpdate.set(k, value);
    });
  }

  /**
   *
   * @returns {Map<string, any>}
   */
  mappingFields(){
    const m = this.client.model;
    const $_POST = this.client.mixin.get('$_POST');
    const fieldsToUpdate = new Map();

    m.fields.forEach((v, k) => {
      const value = $_POST[`:${k}`];
      if(value === undefined)return;
      if(Array.isArray(value))return;

      fieldsToUpdate.set(k, value);
    });

    return fieldsToUpdate;
  }

  /**
   *
   * @param {Map} fieldsToUpdate
   * @returns {ORM}
   */
  update(fieldsToUpdate){
    const m = this.client.model;
    const mm = new m(this.client.id, {database : this.clientDB});
    this.addBehaviour('oldInstance', Object.assign({}, mm));

    Object.assign(mm, Object.fromEntries(fieldsToUpdate));
    mm.save();

    return mm;
  }

  create(fieldsToUpdate){
    const m = this.client.model;
    const mm = new m(null, {database : this.clientDB});
    Object.assign(mm, Object.fromEntries(fieldsToUpdate));
    mm.save();

    return mm;
  }

  add(m, mID, postData, isAppend=false){
    if(!m.belongsToMany || m.belongsToMany.length <= 0)return;

    m.belongsToMany.forEach(modelName => {
      const values = postData[':' + modelName];
      if(values === undefined)return;
      if(!Array.isArray(values))return;

      const model = K8.require(`models/${modelName}`);
      const lk = m.jointTablePrefix + '_id';
      const table = `${m.jointTablePrefix}_${model.tableName}`;

      const filteredValues = values.filter(x => (x !== ""));

      //nothing to add.. quit
      if(filteredValues.length <= 0)return;

      //remove
      if(!isAppend){
        this.clientDB.prepare(`DELETE FROM ${table} WHERE ${lk} = ?`).run(mID);
      }

      //add
      this.clientDB.prepare(`INSERT OR IGNORE INTO ${table} VALUES ${filteredValues.map( (x, i) => `(${mID}, ?, ${i})`).join(', ')}`).run(...filteredValues);

    });
  }

  collectDeepChildren(postData){
    const result = new Map();
    const pattern = /^(([\w/]+)\(\)>)*([\w/]+)\(\):([^\(\):]+$)/;
    Object.keys(postData).forEach(rawKey => {
      const matches = pattern.exec(rawKey);
      if(!matches)return;

      const prevType = matches[2] || '';
      const type     = matches[3];
      const prop     = matches[4];
      const value    = postData[rawKey];

      const instancesPrevType   = readOrCreateMap(result, prevType, new Map());
      const instancesType       = readOrCreateMap(instancesPrevType, type, {});

      if(value==='')return;
      instancesType[prop] = value;
    })

    return result;
  }

  collectPostDataInstances(postData){
    //accept data format:
    //instance(id):field
    //eg: Inventory(2013102013):quantity

    //filter models data
    const instances = new Map();
    const pattern = /^(([\w/]+)\((\d+)\)):([^\(\):]+$)/;
    //2,3,4

    Object.keys(postData).forEach(rawKey => {

      const matches = pattern.exec(rawKey);
      if(!matches)return;

      //validate field ?
      const type = matches[2];
      const id = matches[3];
      const prop = matches[4];
      const value = postData[rawKey];

      //instances[type][id][prop] = value

      const instancesType   = readOrCreateMap(instances, type, new Map());
      const instancesTypeId = readOrCreateMap(instancesType, id, {});
      instancesTypeId[prop] = value;
    });

    return instances;
  }

  collectManyToManyForeign(postData, localModel, localID){
    /*convert postData["*ModelA"] => [["ModelA", {ids: [id], postData: {":Model":[]}} ]]*/
    const result = new Map();
    const pattern = /^\*([A-Z][a-z0-9_]+)$/;

    Object.keys(postData).forEach(rawKey => {
      const matches = pattern.exec(rawKey);
      if(!matches)return;
      if(!Array.isArray(postData[rawKey]))return;
      const filteredValues = postData[rawKey].filter(x => (x !== ""));
      if(filteredValues.length <= 0)return;

      const obj = {}
      obj[`:${localModel.name}`] = [localID];

      result.set(pluralize.singular(matches[1]), {ids: postData[rawKey], postData: obj });
    })

    return result;
  }

  multipleUpdate(instances){
    instances.forEach((idValuesMap, type) => {
      //the id must exist
      const Model = K8.require(`models/${type}`);
      const ids = new Set([...idValuesMap.keys()]);
      ids.delete('');

      const results = this.clientDB.prepare(`SELECT * FROM ${Model.tableName} WHERE id in (${[...ids].join(', ')})`).all();

      results.forEach(record => {
        const newValues = idValuesMap.get(record.id.toString());

        //check value, if diff, save;
        if (!equal(record, Object.assign({}, record, newValues))) {
          const instance = new Model(record.id, {database: this.clientDB, lazyload: true});
          Object.assign(instance, record, newValues);

          instance.save();
        }
      });
    });
  }


  /**
   *
   * @param {ORM} Model
   * @param {Map} children
   */
  validateChildren(Model, children){
    const acceptModels = new Set(Model.hasMany.map( x => x[1]));

    [...children.keys()].forEach( (modelName) => {
      if( !acceptModels.has(modelName) ){
        throw new Error(`Invalid POST data. ${Model.name} not has many ${modelName}`);
      }
    })
  }

  /**
   * @param {map} instances
   */
  validateModel(instances){
    const model = this.client.model;
    if(!model)return;

    [...instances.keys()].forEach( modelName => {
      if(modelName !== model.name){
        throw new Error(`Invalid POST data. Controller only accept ${model.name} for multiple update. Your POST Data contains ${modelName}.`)
      }
    });
  }

  validateNoEmptyId(instances){
    instances.forEach((v, model) =>{
      if(v.has('')){
        throw new Error(`Empty Id is not allowed. ${model}() found.`)
      }
    })
  }

  createHasMany(type, values, parent){
    //the id must exist
    const Model = K8.require(`models/${type}`);

    //find belongs fk
    const modelFKMap = new Map(Array.from(Model.belongsTo).map(x => [x[1], x[0]]));

    const valuesMap = new Map(Object.entries(values))
    valuesMap.forEach((v, k) =>{
      if(v === '')valuesMap.delete(k);
    })

    //nothing to write
    if(valuesMap.size === 0)return;

    valuesMap.set(
      modelFKMap.get(parent.constructor.name).toString(),
      parent.id
    )

    const mm = new Model(null, {database : this.clientDB});
    Object.assign(mm, Object.fromEntries(valuesMap) );

    mm.save();
    return mm;
  }
}

module.exports = ControllerMixinORMWrite;