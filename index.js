const {K8} = require('@komino/k8');
K8.addNodeModules(require.resolve('./'));

module.exports={
  Auth : require('./classes/Auth'),
  ControllerMixinAdminActionLogger: require('./classes/controller-mixin/AdminActionLogger'),
  ControllerMixinORMEdit : require('./classes/controller-mixin/ORMEdit'),
  ControllerMixinORMWrite : require('./classes/controller-mixin/ORMWrite'),
  ControllerMixinLoginRequire : require('./classes/controller-mixin/LoginRequire'),
  ControllerMixinAdmin: require('./classes/controller-mixin/Admin'),
  ControllerMixinCRUDRedirect: require('./classes/controller-mixin/CRUDRedirect'),
  ControllerMixinProxyTable: require('./classes/controller-mixin/ProxyTable'),
}