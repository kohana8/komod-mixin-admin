const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Media extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;
    //fields
    this.url = null;
  }
}

Media.jointTablePrefix = 'media';
Media.tableName = 'media';

Media.fields = new Map([
["url", "String!"],
]);

Media.belongsTo = new Map([
]);

Media.hasMany = [
];

Media.belongsToMany = [
];

module.exports = Media;
