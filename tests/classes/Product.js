const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Product extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //fields
    this.name = null;
  }
}

Product.jointTablePrefix = 'product';
Product.tableName = 'products';

Product.fields = new Map([
["name", "String"],
]);

Product.belongsTo = new Map([
]);

Product.hasMany = [
];

Product.belongsToMany = [
"Media",
];

module.exports = Product;
