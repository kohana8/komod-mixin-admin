const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class ModelTest extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;
    this.name = null;
  }
}

ModelTest.jointTablePrefix = 'test';
ModelTest.tableName = 'tests';

ModelTest.fields = new Map([
  ["name", "String!"]
]);

ModelTest.belongsTo = new Map([
]);

ModelTest.hasMany = [

];

ModelTest.belongsToMany = [

];

module.exports = ModelTest;
