const {Controller} = require('@komino/k8');

module.exports = {
  create: (action="", model= require('./ModelTest'), mixins = [], mixinKVs= new Map(), id= null) => (class T extends Controller{
    constructor(request) {
      super(request);
      this.id = id;
      this.mixin.set('sql', []);
      this.mixin.set('dbRun', []);
      this.mixin.set('dbGet', []);
      this.mixin.set('dbAll', []);

      mixinKVs.forEach((v, k)=>{
        this.mixin.set(k, v);
      })

      this.mixin.set('db', {
        prepare: sql => {
          this.mixin.get('sql').push(sql);
          return {
            run : (...x) => {
              this.mixin.get('dbRun').push(x);
              return null;
            },
            get : (...x) => {
              this.mixin.get('dbGet').push(x);
              return null;
            },
            all : (...x) => {
              this.mixin.get('dbAll').push(x);
              return [];
            },
          }
        },
      });

      mixins.forEach(MX => {
        this.addMixin(new MX(this))
      })

      this.model = model;

      this['action_' + action] = async () => {

      }
    }

    async after(){
      this.body = {
        mixin: [...this.mixin]
      };
      await super.after();
    }
  })
}