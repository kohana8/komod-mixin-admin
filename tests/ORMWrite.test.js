const path = require('path');
const {K8} = require('@komino/k8');
K8.init();
K8.classPath['helper/Form.js']      = path.normalize(__dirname + '/../../mixin-orm/classes/helper/Form');
K8.classPath['models/ModelTest.js'] = path.normalize(__dirname + '/classes/ModelTest');
K8.classPath['models/Person.js']    = path.normalize(__dirname + '/../../admin/classes/models/Person');
K8.classPath['models/User.js']      = path.normalize(__dirname + '/../../admin/classes/models/User');
K8.classPath['models/Role.js']      = path.normalize(__dirname + '/../../admin/classes/models/Role');

K8.classPath['models/Product.js']    = path.normalize(__dirname + '/classes/Product');
K8.classPath['models/Media.js']    = path.normalize(__dirname + '/classes/Media');

const ControllerMixinORMWrite = require('../classes/controller-mixin/ORMWrite');

describe('ORM write test', ()=>{
  const ControllerFactory = require('./classes/ControllerFactory');
  const {RouteList} = require('@komino/komod-route');
  const Person = K8.require('models/Person');
  const ModelTest = K8.require('models/ModelTest');
  const Product = K8.require('models/Product');
  const Media = K8.require('models/Media');

  beforeEach(()=>{
    this.app = require('fastify')();
  })

  test('multiple update', async () => {
    const route = '/test2';
    const action = 'multiple_update';
    const postData = {
      'ModelTest(2401110702891):name' : '111',
      'ModelTest(2401110710563):name' : '222'
    }
    const Controller = ControllerFactory.create(action, ModelTest, [ControllerMixinORMWrite], new Map([['$_POST', postData]]));
    RouteList.add(route, Controller, action)
    RouteList.createRoute(this.app);

    //execute
    const result = await this.app.inject({url: route});

    const body = JSON.parse(result.body);
    body.mixin = new Map(body.mixin);
    if(result.statusCode !== 200){
      console.log(body);
    }
    expect(result.statusCode).toBe(200);
    expect(body.mixin.get('sql')[0]).toBe('SELECT * FROM tests WHERE id in (2401110702891, 2401110710563)')
  })

  test('action update', async () =>{
    const route = '/test1';
    const action = 'update';
    const Controller = ControllerFactory.create(action, ModelTest, [ControllerMixinORMWrite], new Map([['$_POST',{":name": "111"}]]));
    RouteList.add(route, Controller, action)
    RouteList.createRoute(this.app);

    let result = await this.app.inject({url: route});

    let body = JSON.parse(result.body);
    body.mixin = new Map(body.mixin);
    expect(body.mixin.get('sql')[0]).toBe('INSERT OR IGNORE INTO tests (name, id) VALUES (?, ?)')
    expect(body.mixin.get('instance').name).toBe("111");
    expect(result.statusCode).toBe(200);
  })

  test('action create person', async()=>{
    const route = '/test3';
    const action = 'update';
    const postData = {
      ':first_name' : 'Bob',
      ':last_name' : 'Chan',
      'User():username' : 'bobbob',
      'User():password' : '#11111111',
    }
    const Controller = ControllerFactory.create(action, Person, [ControllerMixinORMWrite], new Map([['$_POST', postData]]));
    RouteList.add(route, Controller, action)
    RouteList.createRoute(this.app);

    let result = await this.app.inject({url: route});

    if(result.statusCode !== 200){
      console.log(result.body);
    }

    let body = JSON.parse(result.body);
    expect(result.statusCode).toBe(200);

    body.mixin = new Map(body.mixin);
    expect(body.mixin.get('sql')[0]).toBe('INSERT OR IGNORE INTO persons (first_name, last_name, phone, email, id) VALUES (?, ?, ?, ?, ?)');
    expect(body.mixin.get('sql')[1]).toBe('INSERT OR IGNORE INTO users (username, password, person_id, role_id, id) VALUES (?, ?, ?, ?, ?)');
    expect(body.mixin.get('dbRun')[0][0]).toBe('Bob');
    expect(body.mixin.get('dbRun')[0][1]).toBe('Chan');
    expect(body.mixin.get('dbRun')[1][0]).toBe('bobbob');
    expect(body.mixin.get('dbRun')[1][1]).toBe('#11111111');

  })

  const setupWithPostData = async (route, action, Model, postData, id) => {
    const Controller = ControllerFactory.create(action, Model, [ControllerMixinORMWrite], new Map([['$_POST', postData]]), id );
    RouteList.add(route, Controller, action);
    RouteList.createRoute(this.app);

    const result = await this.app.inject({url: route});
    result.body = JSON.parse(result.body);
    result.body.mixin = new Map(result.body.mixin);
    return result;
  }

  test('action update person with id', async()=>{
    const result = await setupWithPostData('/test4', 'update', Person,
      {
        ':first_name' : 'Bob',
        ':last_name' : 'Ban',
      }, 1
    );

    if(result.statusCode !== 200){
      console.log(result.body);
    }

    const sqls = result.body.mixin.get('sql');
    const gets = result.body.mixin.get('dbGet');
    const runs = result.body.mixin.get('dbRun');
    expect(sqls[0]).toBe('SELECT * from persons WHERE id = ?');
    expect(JSON.stringify(gets[0])).toBe('[1]');
    expect(sqls[1]).toBe('UPDATE persons SET first_name = ?, last_name = ?, phone = ?, email = ? WHERE id = ?');
    expect(JSON.stringify(runs[0])).toBe(`["Bob","Ban",null,null,1]`)
  })

  test('action update person with id and new child', async()=>{
    const result = await setupWithPostData('/test5', 'update', Person,
      {
        ':first_name' : 'Alice',
        ':last_name' : 'Chan',
        'User():username' : 'alicechan',
        'User():password' : '#2222222',
      }, 10
    );

    const sqls = result.body.mixin.get('sql');
    const gets = result.body.mixin.get('dbGet');
    const runs = result.body.mixin.get('dbRun');
    expect(sqls[0]).toBe('SELECT * from persons WHERE id = ?');
    expect(JSON.stringify(gets[0])).toBe('[10]');
    expect(sqls[1]).toBe('UPDATE persons SET first_name = ?, last_name = ?, phone = ?, email = ? WHERE id = ?');
    expect(runs[0][0]).toBe("Alice");
    expect(runs[0][1]).toBe("Chan");
    expect(runs[0][4]).toBe(10);
    expect(sqls[2]).toBe('INSERT OR IGNORE INTO users (username, password, person_id, role_id, id) VALUES (?, ?, ?, ?, ?)');
    expect(runs[1][0]).toBe("alicechan");
    expect(runs[1][1]).toBe('#2222222');
    expect(runs[1][2]).toBe(10);
    expect(runs[1][3]).toBe(null);
  });

  test('create ORM with many to many', async()=>{
    const result = await setupWithPostData('/test6', 'update', Product, {
      ":name" : "Foo",
      ":Media": ["1", "2"]
    });

    const sqls = result.body.mixin.get('sql');
    const gets = result.body.mixin.get('dbGet');
    const runs = result.body.mixin.get('dbRun');
    const newProductId = runs[0][1];
    expect(sqls[0]).toBe('INSERT OR IGNORE INTO products (name, id) VALUES (?, ?)');
    expect(runs[0][0]).toBe("Foo");
    expect(sqls[1]).toBe('DELETE FROM product_media WHERE product_id = ?');
    expect(runs[1][0]).toBe(runs[0][1]);
    expect(sqls[2]).toBe('INSERT INTO product_media VALUES ('+ newProductId + ', ?, 0), ('+ newProductId + ', ?, 1)');
    expect(runs[2][0]).toBe('1');
    expect(runs[2][1]).toBe('2');
  })

  test('create ORM with entity many to many', async()=>{
    const result = await setupWithPostData('/test7', 'update', Media, {
      ":url" : "http://example.com",
      "*Product": ["111"]
    });

    const sqls = result.body.mixin.get('sql');
    const gets = result.body.mixin.get('dbGet');
    const runs = result.body.mixin.get('dbRun');

    const newId = runs[0][1];
    expect(sqls[0]).toBe('INSERT OR IGNORE INTO media (url, id) VALUES (?, ?)');
    expect(runs[0][0]).toBe("http://example.com");
    expect(sqls[1]).toBe('DELETE FROM product_media WHERE product_id = ?');
    expect(runs[1][0]).toBe('111');
    expect(sqls[2]).toBe('INSERT INTO product_media VALUES (111, ?, 0)');
    expect(runs[2][0]).toBe(newId);

  })

  test('createHasMany but values are not availabe', async() =>{
    const result = await setupWithPostData('/test8', 'update', Person,
      {
        ':first_name' : 'Alice',
        ':last_name' : 'Chan',
        'User():foo' : '',
        'User():bar' : '',
      }, 10
    );
    expect(result.statusCode).toBe(200);
  })

  test('belongsToFields', async()=>{

  })
})